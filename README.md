Thermos - a craftbukkit forge server for Minecraft
==================================================
You can find more about on [Thermos repository][thermos-repo].

In documentation below assume that 'thermos' and 'thermos-data' is main and data container names respectively.

To start image:
---------------
```shell
docker run -t -[i] -[d] -p 25565:25565 [--{host,}name=thermos] aranokai/openjdk-minecraft_thermos[:<tag>] [<jvm_opt>]
```
*latest* tag will point to latest build with most new forge.

By specifying any `<jvm_opt>` You **override all default** JVM options, be careful. For list default JVM options:
```shell
docker inspect -f '{{.Config.Cmd}}' aranokai/openjdk-minecraft_thermos
```
There is also strict JVM options lurking nearby, which You can list:
```shell
docker inspect -f '{{range .Config.Env}}{{println .}}{{end}}' aranokai/openjdk-minecraft_thermos | grep JVM_OPT_STRICT
```
This will be applied to JVM in any case. You still can (but not supposed to) override it by specifying `-e JVM_OPT_STRICT=<opt>` in docker options.

To start with data container:
-----------------------------
Creating a data container will simplify upgrade, retain emotional balance ~~and clean karma~~ (:
All Your configs/worlds/etc will be stored in data container, so You can safely switch version or even delete main container!
Create some if You still don't have any:
```shell
docker create --{host,}name=thermos-data aranokai/openjdk-minecraft_thermos
```
Then use `--volumes-from thermos-data` docker option in start sequence and backup/restore processes.

*NOTE*: if You already start **WITHOUT** data container, **make backup** with `--volumes-from thermos` and, after creation of data container, **restore** with `--volumes-from thermos-data` to preserve changes.

To backup:
----------
```shell
docker run --rm --volumes-from thermos --user root --entrypoint tar -v $(pwd):/backup aranokai/openjdk-minecraft_thermos czf /backup/thermos-backup.tgz ./
						  #change ↑ to thermos-data if using data container
```
This will create `thermos-backup.tgz` in Your current directory.

To restore:
-----------
```shell
docker run --rm --volumes-from thermos --user root --entrypoint tar -v $(pwd):/backup aranokai/openjdk-minecraft_thermos xzf /backup/thermos-backup.tgz
						  #change ↑ to thermos-data if using data container
```
This will restore content from `thermos-backup.tgz` in Your current directory.

There is a possibility of divergence in the permissions of current container and backup. To fix it:
```shell
docker run --rm --volumes-from thermos --user root --entrypoint sh aranokai/openjdk-minecraft_thermos -c 'chown -R ${THERMOS_USR}.${THERMOS_USR} ${THERMOS_WORKDIR}'
						  #change ↑ to thermos-data if using data container
```

Add config/world/etc:
--------------------
```shell
docker cp <local_path> thermos:./<where_to_store>/
				  #change ↑ to thermos-data if using data container
```
Swap the `cp` arguments to get items back.

To get server console:
------------------------
```shell
docker exec -ti thermos tmux attach -t MCConsole
```
Use 'CTRL-b d' key sequence to go back to Your precious shell.

To upgrade/rollback Thermos version:
------------------------------------
Make backup or use data container, then:
```shell
docker pull aranokai/openjdk-minecraft_thermos:<desired_tag_here>
```
Restore backup/run with data container.

SystemD service:
---------------
[Here is simple systemd service file][systemd-service]
Copy it in `/etc/systemd/system/` and:
```shell
systemctl daemon-reload
systemctl start MCThermos.service
```

Build your own:
--------------
```shell
git clone https://bitbucket.org/Arano-kai/docker-openjdk-minecraft_thermos.git
cd docker-openjdk-minecraft_thermos/buildroot
<happily hacking...>
docker build -t my_thermos_server .
```

Improve something:
------------------
Make pull request :)

Do not get upset by long response - sometimes I sorely lacking of time...

[thermos-repo]: https://github.com/CyberdyneCC/Thermos
[systemd-service]: https://bitbucket.org/Arano-kai/docker-openjdk-minecraft_thermos/raw/stable/utils/docker-MCThermos.service
